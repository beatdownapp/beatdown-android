/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

import com.beatdown.client.R;
import com.beatdown.client.configuration.LocaleHelper;
import com.beatdown.client.ui.notification.NotificationActivity;
import com.beatdown.client.utils.JsonUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    /**
     * Listener de les notificacions push.
     * @param message
     */
    @Override
    public void onMessageReceived(RemoteMessage message) {
        super.onMessageReceived(message);

        JsonObject obj = JsonUtil.getJsonObject(message.getNotification().getBody());
        String msg = obj.get(LocaleHelper.getLanguage(getApplicationContext())).getAsString();

        // Intent a pantalla de notificacions
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), (int) (System.currentTimeMillis() & 0xff), new Intent(this, NotificationActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "default")
                .setContentTitle(getString(getApplicationContext().getResources().getIdentifier(message.getNotification().getTitle(), "string", getApplicationContext().getPackageName())))
                .setContentText(msg)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.drawable.scissors_small)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}