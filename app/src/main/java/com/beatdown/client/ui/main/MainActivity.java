/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beatdown.client.R;
import com.beatdown.client.api.DefaultApi;
import com.beatdown.client.configuration.AccountHelper;
import com.beatdown.client.configuration.ConfigurationHelper;
import com.beatdown.client.configuration.LocaleHelper;
import com.beatdown.client.model.Offer;
import com.beatdown.client.ui.configuration.AccountConfigActivity;
import com.beatdown.client.ui.configuration.SearchConfigActivity;
import com.beatdown.client.ui.login.LoginActivity;
import com.beatdown.client.ui.main.adapters.MainAdapter;
import com.beatdown.client.ui.main.adapters.MarkerInfoWindowAdapter;
import com.beatdown.client.ui.notification.NotificationActivity;
import com.beatdown.client.ui.offer.FavoriteOfferActivity;
import com.beatdown.client.ui.offer.OfferDetailActivity;
import com.beatdown.client.ui.offer.PersonalOfferActivity;
import com.beatdown.client.utils.ImageUtil;
import com.beatdown.client.utils.ResourcesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, NavigationView.OnNavigationItemSelectedListener, GoogleMap.OnInfoWindowClickListener, MainAdapter.RecyclerViewClickListener {

    private GoogleMap mMap;
    private ConstraintLayout progressBar;
    private ActionBarDrawerToggle toggle;
    public static List<Offer> offerList;
    private ImageView navHeaderAvatar;
    private TextView navHeaderEmail;
    public static final String UID_MAIN = "main";
    private RecyclerView rvMain;
    private FrameLayout mainFrameLay;
    private static RecyclerView.Adapter mAdapter;
    private static boolean MAP_VIEW = true;

    // Permisos
    private static final int PERMISSIONS_CODE = 10;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};

    // Location
    private LocationManager locationManager;
    public static double latitude, longitude;
    private static final double MOCK_LAT = 41.372833;
    private static final double MOCK_LONG = 2.148306;

    // Proves
    private static final boolean TEST = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        // Progress bar
        progressBar = findViewById(R.id.progressBar);

        Toolbar mTopToolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(mTopToolbar);

        // Progress bar
        progressBar = findViewById(R.id.progressBar);

        showProgress(true);

        // Ajustem menú
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        toggle = new ActionBarDrawerToggle(this, drawer, mTopToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // Listener menú lateral
        NavigationView navigationView = findViewById(R.id.nav_view);
        navHeaderEmail = navigationView.getHeaderView(0).findViewById(R.id.nav_header_email);
        navHeaderAvatar = navigationView.getHeaderView(0).findViewById(R.id.nav_header_avatar);
        navigationView.setNavigationItemSelectedListener(this);

        // Recycler view i frame layout
        rvMain = findViewById(R.id.main_rv_list);
        mainFrameLay = findViewById(R.id.main_framelay);

        // Comprovem usuari logejat
        checkLoggedUser(navigationView.getMenu());

        // Listener menú bottom
        BottomNavigationView bottomNavigationView = findViewById(R.id.main_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.main_item_bottom_list:
                        MAP_VIEW = false;
                        break;
                    case R.id.main_item_bottom_map:
                        MAP_VIEW = true;
                        break;
                }
                return showView();
            }
        });

        initFirebase();

        // Permisos
        checkPermissions();

        // Location
        initLocation();
    }

    /**
     * Subscrivim app al topic "app".
     */
    private void initFirebase() {
        FirebaseMessaging.getInstance().subscribeToTopic("app")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d(MainActivity.class.getSimpleName(), "Subscribed!");
                    }
                });
    }

    /**
     * Decideix què mostrar.
     */
    private boolean showView() {
        if (MAP_VIEW) {
            mainFrameLay.setVisibility(View.VISIBLE);
            rvMain.setVisibility(View.GONE);
        } else {
            showProgress(true);
            loadRecyclerView();
            mainFrameLay.setVisibility(View.GONE);
            rvMain.setVisibility(View.VISIBLE);
        }

        return true;
    }

    /**
     * Càrrega de recycler view.
     */
    private void loadRecyclerView() {
        // Ajustem dades
        mAdapter = new MainAdapter(offerList, getApplicationContext(), this);

        // Adapter
        rvMain.setAdapter(mAdapter);

        // Use a linear layout manager
        rvMain.setLayoutManager(new GridLayoutManager(this, 2));

        showProgress(false);
    }

    /**
     * Inicialització de location.
     */
    private void initLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        String mprovider = locationManager.getBestProvider(criteria, false);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Location location = getLastKnownLocation();
        locationManager.requestLocationUpdates(mprovider, 15000, 1, this);

        if (location != null)
            onLocationChanged(location);
    }

    /**
     * Crida quan el mapa està llest.
     *
     * @param googleMap
     */
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Configuració mapa
        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setCompassEnabled(false);
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);

        // Habilitem localització
        mMap.setMyLocationEnabled(true);

        // Listener marker
        mMap.setOnInfoWindowClickListener(this);

        // Adapter per info window
        mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter(this));

        // Carreguem markers
        loadMarkersOnScreen();
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    /**
     * Crida a API per càrrega de marcadors.
     */
    private void getMarkers() {

        showProgress(true);

        final Response.Listener<List<Offer>> responseListener = new Response.Listener<List<Offer>>() {
            @Override
            public void onResponse(List<Offer> offerList_) {
                showProgress(false);
                offerList = offerList_;
                loadMap();
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    ResourcesUtil.showError(getApplicationContext(), getApplicationContext().getPackageName(), response.data);
                }
            }
        };

        // Enviem petició
        new DefaultApi().getMapOffers(ConfigurationHelper.getDistance(getApplicationContext()), BigDecimal.valueOf(latitude), BigDecimal.valueOf(longitude), AccountHelper.hasUserData(getApplicationContext()) ? Integer.valueOf(AccountHelper.getUserId(getApplicationContext())) : null, ConfigurationHelper.getCategories(getApplicationContext()), responseListener, errorListener);
    }

    /**
     * Càrrega de mapa en fragment.
     */
    private void loadMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map2);

        if (mapFragment != null)
            mapFragment.getMapAsync(this);
    }

    private void showProgress(boolean show) {
        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    /**
     * Comprova els permisos GPS.
     */
    private void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<>();

        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }

        if (!missingPermissions.isEmpty()) {
            final String[] permissions = missingPermissions.toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, PERMISSIONS_CODE);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(PERMISSIONS_CODE, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    /**
     * Reposta al permís GPS. Tanquem app si es rebutgen.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_CODE:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        finish();
                        return;
                    }
                }
                initLocation();
                break;
        }
    }

    private void loadMarkersOnScreen() {
        // Afegir markers d'ofertes
        int i = 0;
        for (Offer offer : offerList) {

            // Ajustem idioma
            String code = LocaleHelper.getLanguage(getApplicationContext());
            String title;
            if (LocaleHelper.EN.equalsIgnoreCase(code))
                title = offer.getTitleEng();
            else if (LocaleHelper.ES.equalsIgnoreCase(code))
                title = offer.getTitleSpa();
            else
                title = offer.getTitleCat();

            // Afegim marker
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(offer.getStablishment().getLatitude().doubleValue(), offer.getStablishment().getLongitude().doubleValue()))
                    .title(title));

            marker.setTag(i);

            i++;
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 14.0f));
    }

    /**
     * Click listener dels marcadors del mapa. Invoquen a la pantalla de detall.
     *
     * @param marker
     * @return
     */
    @Override
    public void onInfoWindowClick(final Marker marker) {
        Intent intent = new Intent(getApplicationContext(), OfferDetailActivity.class);
        intent.putExtra(Intent.EXTRA_TEXT, (int) marker.getTag());
        intent.putExtra(Intent.EXTRA_UID, MainActivity.UID_MAIN);
        startActivity(intent);
    }

    /**
     * Recuperem long i lat.
     *
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        longitude = TEST ? MOCK_LONG : location.getLongitude();
        latitude = TEST ? MOCK_LAT : location.getLatitude();

        // Carreguem markers
        getMarkers();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // Not used
    }

    @Override
    public void onProviderEnabled(String provider) {
        // Not used
    }

    @Override
    public void onProviderDisabled(String provider) {
        // Not used
    }

    /**
     * Retorna localització.
     *
     * @return
     */
    private Location getLastKnownLocation() {
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;

        for (String provider : providers) {
            @SuppressLint("MissingPermission") Location l = locationManager.getLastKnownLocation(provider);

            if (l == null) {
                continue;
            }
            if (bestLocation == null
                    || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        if (bestLocation == null) {
            return null;
        }
        return bestLocation;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.menu_configuration:
                startActivity(new Intent(getApplicationContext(), AccountConfigActivity.class));
                break;
             case R.id.menu_notifications:
                startActivity(new Intent(getApplicationContext(), NotificationActivity.class));
                break;
            case R.id.menu_favorite:
                startActivity(new Intent(getApplicationContext(), FavoriteOfferActivity.class));
                break;
            case R.id.menu_personal:
                startActivity(new Intent(getApplicationContext(), PersonalOfferActivity.class));
                break;
            case R.id.menu_signout:
                signOut();
                break;
        }

        return false;
    }

    /**
     * Omplim menú superior.
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_top, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Listener quan es clica l'opció.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_main_top_configuration) {
            Intent intent = new Intent(getApplicationContext(), SearchConfigActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Mètode encarregat de desactivar funcions per usuaris no connectats.
     */
    private void checkLoggedUser(Menu menu) {

        boolean hasLoggedUser = AccountHelper.hasUserData(getApplicationContext());

        if (!hasLoggedUser) {
            menu.findItem(R.id.menu_signout).setVisible(false);
            menu.findItem(R.id.menu_personal).setVisible(false);
            menu.findItem(R.id.menu_favorite).setVisible(false);
            menu.findItem(R.id.menu_notifications).setVisible(false);
        } else {
            // Avatar
            String avatar = AccountHelper.getUserAvatar(getApplicationContext());
            String email = AccountHelper.getUserEmail(getApplicationContext());

            if (avatar != null)
                navHeaderAvatar.setImageBitmap(ImageUtil.fromBase64ToBitmap(avatar));

            // Email
            navHeaderEmail.setText(email);
        }
    }

    /**
     * Funció encarregada de netejar dades de l'usuari i tornar al login.
     */
    private void signOut() {
        AccountHelper.eraseUserData(getApplicationContext());
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finishAffinity();
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        Intent intent = new Intent(getApplicationContext(), OfferDetailActivity.class);
        intent.putExtra(Intent.EXTRA_TEXT, position);
        intent.putExtra(Intent.EXTRA_UID, UID_MAIN);
        startActivity(intent);
    }
}
