/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.offer.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beatdown.client.R;
import com.beatdown.client.configuration.LocaleHelper;
import com.beatdown.client.model.Offer;

import java.util.List;

public class FavoriteOfferAdapter extends RecyclerView.Adapter<FavoriteOfferAdapter.MyViewHolder> {

    private List<Offer> mDataset;
    private Context context;
    private static RecyclerViewClickListener itemListener;

    public interface RecyclerViewClickListener {
        void recyclerViewListClicked(View v, int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title, desc;

        MyViewHolder(LinearLayout v) {
            super(v);
            title = v.findViewById(R.id.favoritediscounts_item_title);
            desc = v.findViewById(R.id.favoritediscounts_item_desc);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());
        }
    }

    public FavoriteOfferAdapter(List<Offer> mDataset, Context context, RecyclerViewClickListener itemListener) {
        this.mDataset = mDataset;
        this.context = context;
        this.itemListener = itemListener;
    }

    // Crea noves views
    @Override
    @NonNull
    public FavoriteOfferAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                                int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favoritediscounts_item, parent, false);

        return new MyViewHolder((LinearLayout) layout);
    }

    // Reemplaça contingut
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        // Ajustem idioma
        String code = LocaleHelper.getLanguage(context);
        String desc, title = mDataset.get(position).getStablishment().getName();

        if (LocaleHelper.EN.equalsIgnoreCase(code))
            desc = mDataset.get(position).getTitleEng();
        else if (LocaleHelper.ES.equalsIgnoreCase(code))
            desc = mDataset.get(position).getTitleSpa();
        else
            desc = mDataset.get(position).getTitleCat();

        holder.desc.setText(desc);
        holder.title.setText(title);
    }

    // Mida del dataset
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}