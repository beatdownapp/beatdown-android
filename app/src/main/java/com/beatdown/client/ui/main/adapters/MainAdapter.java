/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.main.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.beatdown.client.R;
import com.beatdown.client.api.DefaultApi;
import com.beatdown.client.configuration.AccountHelper;
import com.beatdown.client.configuration.LocaleHelper;
import com.beatdown.client.model.Offer;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MyViewHolder> {

    private List<Offer> mDataset;
    private Context context;
    private static RecyclerViewClickListener itemListener;

    public interface RecyclerViewClickListener {
        void recyclerViewListClicked(View v, int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView image, fav;
        TextView title, category, desc, discount;

        MyViewHolder(ConstraintLayout view) {
            super(view);
            image = view.findViewById(R.id.mainitem_image);
            fav = view.findViewById(R.id.mainitem_button_fav);
            title = view.findViewById(R.id.mainitem_label_title);
            category = view.findViewById(R.id.mainitem_label_category_distance);
            desc = view.findViewById(R.id.mainitem_label_description);
            discount = view.findViewById(R.id.mainitem_label_discount);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());
        }
    }

    public MainAdapter(List<Offer> mDataset, Context context, RecyclerViewClickListener itemListener) {
        this.mDataset = mDataset;
        this.context = context;
        this.itemListener = itemListener;
    }

    // Crea noves views
    @Override
    @NonNull
    public MainAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                       int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_activity_item, parent, false);

        return new MyViewHolder((ConstraintLayout) layout);
    }

    // Reemplaça contingut
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        // Obtenim oferta
        Offer offer = mDataset.get(position);

        // Labels
        holder.title.setText(offer.getStablishment().getName());

        String cat = offer.getStablishment().getCategory().getDescriptionCat();
        String descr = offer.getTitleCat();

        // Traduïm segons idioma
        if (LocaleHelper.getLanguage(context).equalsIgnoreCase(LocaleHelper.EN)) {
            cat = offer.getStablishment().getCategory().getDescriptionEng();
            descr = offer.getTitleEng();
        } else if (LocaleHelper.getLanguage(context).equalsIgnoreCase(LocaleHelper.ES)) {
            cat = offer.getStablishment().getCategory().getDescriptionSpa();
            descr = offer.getTitleSpa();
        }

        holder.category.setText(String.format("%s · %s km", cat, offer.getDistance()));
        holder.desc.setText(descr);

        // Tractem el tipus de descompte
        String discountV = offer.getDiscount() + "";

        if (discountV.startsWith("-"))
            discountV += "€";
        else
            discountV += "%";

        holder.discount.setText(discountV);

        // Imatges
        Picasso.get().load(DefaultApi.basePath + offer.getImageUrl()).into(holder.image);
        if (AccountHelper.hasUserData(context))
            holder.fav.setImageResource(offer.getFavorite() ? R.drawable.button_fav : R.drawable.button_fav_unmarked);
        else
            holder.fav.setVisibility(View.INVISIBLE);
    }

    // Mida del dataset
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}