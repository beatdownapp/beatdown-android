/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.login;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beatdown.client.R;
import com.beatdown.client.api.DefaultApi;
import com.beatdown.client.configuration.AccountHelper;
import com.beatdown.client.configuration.LocaleHelper;
import com.beatdown.client.model.User;
import com.beatdown.client.ui.account.RecoverActivity;
import com.beatdown.client.ui.account.SignUpActivity;
import com.beatdown.client.ui.main.MainActivity;
import com.beatdown.client.utils.ResourcesUtil;

import java.util.Locale;

public class LoginActivity extends AppCompatActivity {

    private TextView linkForgot, linkReg, linkNoSession;
    private EditText email, password;
    private Button sendButton;
    private ConstraintLayout progressBar;
    private boolean emailOk = false, passwordOk = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        loadLanguageApp();

        // Abans de carregar alguna cosa, comprovem si ja hi ha alguna sessió creada
        checkSession();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        // Elements
        linkForgot = this.findViewById(R.id.login_link_forgot);
        linkReg = this.findViewById(R.id.login_link_register);
        linkNoSession = this.findViewById(R.id.login_link_without_signin);
        email = findViewById(R.id.login_field_email);
        password = findViewById(R.id.login_field_password);
        sendButton = findViewById(R.id.login_button_signin);
        progressBar = findViewById(R.id.progressBar);

        // Link
        linkForgot.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), RecoverActivity.class);
                startActivity(intent);
            }
        });

        linkReg.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(intent);
            }
        });

        linkNoSession.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finishActivity();
            }
        });

        // Listeners
        email.addTextChangedListener(new TextWatcher()  {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s)  {
                if (!TextUtils.isEmpty(email.getText().toString()) && !Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
                    enableSendButton(false);
                    emailOk = false;
                } else {
                    if (TextUtils.isEmpty(email.getText().toString())) {
                        enableSendButton(false);
                        emailOk = false;
                    } else {
                        emailOk = true;
                        checkFields();
                    }
                }
            }
        });

        password.addTextChangedListener(new TextWatcher()  {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s)  {
                if (TextUtils.isEmpty(password.getText().toString())) {
                    enableSendButton(false);
                    passwordOk = false;
                } else {
                    passwordOk = true;
                    checkFields();
                }
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                login();
            }
        });

    }

    private void login() {

        final Response.Listener<User> responseListener = new Response.Listener<User>() {
            @Override
            public void onResponse(User response) {
                showProgress(false);

                // Guardem les dades de l'usuari
                AccountHelper.setUserData(getApplicationContext(), response.getEmail(), response.getUserId().toString(), response.getAvatar());

                // Anem a pantalla principal un cop OK
                loadMain();
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                    ResourcesUtil.showError(getApplicationContext(), getApplicationContext().getPackageName(), response.data);
                }
            }
        };

        // Enviem petició
        new DefaultApi().login(email.getText().toString(), password.getText().toString(), responseListener, errorListener);

    }

    private void checkSession() {

        // Comprovem si hi han dades guardades
        if (AccountHelper.hasUserData(getApplicationContext()))
            loadMain();
    }

    private void enableSendButton(boolean enable) {
        sendButton.setEnabled(enable);
    }

    private void enableFields(boolean enable) {
        email.setEnabled(enable);
        password.setEnabled(enable);
    }

    private void checkFields() {
        if (passwordOk && emailOk)
            enableSendButton(true);
    }

    private void showProgress(boolean show) {
        enableSendButton(!show);
        enableFields(!show);

        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    private void finishActivity() {
        this.finishAffinity();
    }

    private void loadMain() {
        // Anem a pantalla principal
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finishActivity();
    }


    /**
     * Mètode que carrega l'idioma de l'app
     */
    private void loadLanguageApp() {
        Locale myLocale = new Locale(LocaleHelper.getLanguage(getApplicationContext()));
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

}
