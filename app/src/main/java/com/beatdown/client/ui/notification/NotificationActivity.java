/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.notification;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beatdown.client.R;
import com.beatdown.client.api.DefaultApi;
import com.beatdown.client.configuration.AccountHelper;
import com.beatdown.client.model.Notification;
import com.beatdown.client.ui.notification.adapters.NotificationAdapter;
import com.beatdown.client.utils.ResourcesUtil;

import java.util.List;

public class NotificationActivity extends AppCompatActivity {

    private RecyclerView notifications;
    private ConstraintLayout progressBar;
    private static List<Notification> notificationList;
    private static RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recyclerview_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Elements
        progressBar = findViewById(R.id.progressBar);
        notifications = findViewById(R.id.recyclerview_list);

        showProgress(true);

        // Càrrega notificacions
        loadNotifications();
    }

    /**
     * Càrrega de recycler view.
     */
    private void loadRecyclerView() {
        // Ajustem dades
        mAdapter = new NotificationAdapter(notificationList, getApplicationContext());

        // Adapter
        notifications.setAdapter(mAdapter);

        // Use a linear layout manager
        notifications.setLayoutManager(new LinearLayoutManager(this));

        showProgress(false);
    }

    /**
     * Recuperem ofertes personals via API.
     */
    private void loadNotifications() {

        final Response.Listener<List<Notification>> responseListener = new Response.Listener<List<Notification>>() {
            @Override
            public void onResponse(List<Notification> response) {
                notificationList = response;
                loadRecyclerView();
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    ResourcesUtil.showError(getApplicationContext(), getApplicationContext().getPackageName(), response.data);
                }
            }
        };

        // Cridem API
        new DefaultApi().getUserNotifications(AccountHelper.getUserId(getApplicationContext()), responseListener, errorListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgress(boolean show) {
        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (notificationList != null)
            mAdapter.notifyDataSetChanged();
    }

}
