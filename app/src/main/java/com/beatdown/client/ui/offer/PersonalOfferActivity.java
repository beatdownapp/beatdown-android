/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.offer;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beatdown.client.R;
import com.beatdown.client.api.DefaultApi;
import com.beatdown.client.configuration.AccountHelper;
import com.beatdown.client.model.PersonalOffer;
import com.beatdown.client.ui.main.MainActivity;
import com.beatdown.client.ui.offer.adapters.PersonalOfferAdapter;
import com.beatdown.client.utils.ResourcesUtil;

import java.math.BigDecimal;
import java.util.List;

public class PersonalOfferActivity extends AppCompatActivity implements PersonalOfferAdapter.RecyclerViewClickListener {

    private RecyclerView offers;
    private ConstraintLayout progressBar;
    protected static List<PersonalOffer> offerList;
    private static RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recyclerview_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Elements
        progressBar = findViewById(R.id.progressBar);
        offers = findViewById(R.id.recyclerview_list);

        showProgress(true);

        // Càrrega ofertes personals
        loadOffers();
    }

    /**
     * Càrrega de recycler view.
     */
    private void loadRecyclerView() {
        // Ajustem dades
        mAdapter = new PersonalOfferAdapter(offerList, getApplicationContext(), this);

        // Adapter
        offers.setAdapter(mAdapter);

        // Use a linear layout manager
        offers.setLayoutManager(new LinearLayoutManager(this));

        showProgress(false);
    }

    /**
     * Recuperem ofertes personals via API.
     */
    private void loadOffers() {

        final Response.Listener<List<PersonalOffer>> responseListener = new Response.Listener<List<PersonalOffer>>() {
            @Override
            public void onResponse(List<PersonalOffer> response) {
                offerList = response;
                loadRecyclerView();
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    ResourcesUtil.showError(getApplicationContext(), getApplicationContext().getPackageName(), response.data);
                }
            }
        };

        // Cridem API
        new DefaultApi().getPersonalOffers(AccountHelper.getUserId(getApplicationContext()), BigDecimal.valueOf(MainActivity.latitude), BigDecimal.valueOf(MainActivity.longitude), responseListener, errorListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.menu_personaloffer_top_qr:
                startActivity(new Intent(getApplicationContext(), QrActivity.class));
                return true;
            case R.id.menu_personaloffer_top_code:
                startActivity(new Intent(getApplicationContext(), PersonalOfferCodeActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgress(boolean show) {
        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    /**
     * Omplim menú superior.
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_personaloffer_top, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (offerList != null)
            mAdapter.notifyDataSetChanged();
    }

    @Override
    public void recyclerViewListClicked(View v, int position){
        Intent intent = new Intent(getApplicationContext(), PersonalOfferDetailActivity.class);
        intent.putExtra(Intent.EXTRA_TEXT, position);
        startActivity(intent);
    }

}
