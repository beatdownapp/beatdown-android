/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.configuration;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beatdown.client.R;
import com.beatdown.client.api.DefaultApi;
import com.beatdown.client.configuration.ConfigurationHelper;
import com.beatdown.client.model.StablishmentCategory;
import com.beatdown.client.ui.configuration.adapters.SearchConfigCategoriesAdapter;
import com.beatdown.client.utils.ResourcesUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SearchConfigActivity extends AppCompatActivity {

    private SeekBar distanceSeekBar;
    private RecyclerView categoriesR;
    private Button sendButton;
    private ConstraintLayout progressBar;
    private TextView hintRange;

    private List<StablishmentCategory> categoryList;

    // Data
    private int distance;
    private Set<String> selectedCategories = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_configuration_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Elements
        sendButton = findViewById(R.id.searchconfig_button_send);
        distanceSeekBar = findViewById(R.id.searchconfig_seekbar_distance);
        progressBar = findViewById(R.id.progressBar);
        categoriesR = findViewById(R.id.searchconfig_list_categories);
        hintRange = findViewById(R.id.searchconfig_hint_range);

        showProgress(true);

        // Comprovem dades guardades
        checkSavedConfig();

        // Listeners
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

        setDistance(distance);

        distanceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                setDistance(progress);
            }
        });

        // Càrrega categories
        loadCategories();
    }

    /**
     * Recupera configuració guardada.
     */
    private void checkSavedConfig() {
        this.distance = ConfigurationHelper.getDistance(getApplicationContext());
        distanceSeekBar.setProgress(distance);

        this.selectedCategories = ConfigurationHelper.getCategories(getApplicationContext());
    }

    /**
     * Ajusta distància.
     *
     * @param progress
     */
    private void setDistance(int progress) {
        hintRange.setText(progress + "km");
        int x = distanceSeekBar.getThumb().getBounds().left;
        hintRange.setX(x);
        distance = progress;
    }

    /**
     * Càrrega de recycler view.
     */
    private void loadRecyclerView() {
        // Ajustem dades
        RecyclerView.Adapter mAdapter = new SearchConfigCategoriesAdapter(categoryList, selectedCategories, getApplicationContext(), new SearchConfigCategoriesAdapter.OnItemCheckListener() {
            @Override
            public void onItemCheck(StablishmentCategory item) {
                selectedCategories.add(item.getStablishmentCategoryId().toString());
            }

            @Override
            public void onItemUncheck(StablishmentCategory item) {
                selectedCategories.remove(item.getStablishmentCategoryId().toString());
            }
        });

        // Adapter
        categoriesR.setAdapter(mAdapter);

        // Use a linear layout manager
        categoriesR.setLayoutManager(new LinearLayoutManager(this));

        showProgress(false);
    }

    /**
     * Guarda la configuració.
     */
    private void save() {
        // Guardem la configuració
        ConfigurationHelper.setSearchConfig(getApplicationContext(), distance, selectedCategories);
        Toast.makeText(getApplicationContext(), R.string.action_msg_saved, Toast.LENGTH_SHORT).show();
    }

    /**
     * Recuperem categories via API.
     */
    private void loadCategories() {

        final Response.Listener<List<StablishmentCategory>> responseListener = new Response.Listener<List<StablishmentCategory>>() {
            @Override
            public void onResponse(List<StablishmentCategory> response) {
                categoryList = response;
                loadRecyclerView();
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    ResourcesUtil.showError(getApplicationContext(), getApplicationContext().getPackageName(), response.data);
                }
            }
        };

        // Cridem API
        new DefaultApi().getAllCategories(responseListener, errorListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgress(boolean show) {
        enableSendButton(!show);

        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    private void enableSendButton(boolean enable) {
        sendButton.setEnabled(enable);
    }
}
