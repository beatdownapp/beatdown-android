/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.offer;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beatdown.client.R;
import com.beatdown.client.api.DefaultApi;
import com.beatdown.client.configuration.AccountHelper;
import com.beatdown.client.model.PersonalOffer;
import com.beatdown.client.ui.main.MainActivity;
import com.beatdown.client.ui.offer.adapters.PersonalOfferAdapter;
import com.beatdown.client.utils.ResourcesUtil;

import java.math.BigDecimal;
import java.util.List;

public class PersonalOfferCodeActivity extends AppCompatActivity {

    private EditText code;
    private Button addButton;
    private ConstraintLayout progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newpersonaldiscount_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Elements
        code = findViewById(R.id.newpersonaldiscount_field_code);
        addButton = findViewById(R.id.newpersonaldiscount_button_add);
        progressBar = findViewById(R.id.progressBar);

        // Listeners
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!code.getText().toString().isEmpty())
                    save();
            }
        });


    }

    /**
     * Guarda a base de dades a través de l'API.
     */
    private void save() {
        showProgress(true);

        final Response.Listener<List<PersonalOffer>> responseListener = new Response.Listener<List<PersonalOffer>>() {
            @Override
            public void onResponse(List<PersonalOffer> response) {
                showProgress(false);
                PersonalOfferAdapter.mDataset = response;
                finish();
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    ResourcesUtil.showError(getApplicationContext(), getApplicationContext().getPackageName(), response.data);
                }
            }
        };

        // Cridem API
        new DefaultApi().createPersonalOffer(AccountHelper.getUserId(getApplicationContext()), code.getText().toString(), BigDecimal.valueOf(MainActivity.latitude), BigDecimal.valueOf(MainActivity.longitude), responseListener, errorListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgress(boolean show) {
        enableSendButton(!show);

        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    /**
     * Omplim menú superior.
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_personaloffer_top, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void enableSendButton(boolean enable) {
        addButton.setEnabled(enable);
    }

}
