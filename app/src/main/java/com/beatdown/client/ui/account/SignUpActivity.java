/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.account;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beatdown.client.utils.JsonUtil;
import com.beatdown.client.R;
import com.beatdown.client.api.DefaultApi;
import com.beatdown.client.configuration.AccountHelper;
import com.beatdown.client.model.User;
import com.beatdown.client.ui.main.MainActivity;
import com.beatdown.client.utils.ResourcesUtil;

public class SignUpActivity extends AppCompatActivity {

    private Button sendButton;
    private EditText email, password, passwordRepeat;
    private CheckBox useConditions, privacyPolicies;
    private ConstraintLayout progressBar;
    private  boolean emailOk = false, passwordOk = false, passwordRepeatOk = false, useConditionsOk = false, privacyPoliciesOk = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Elements
        sendButton = findViewById(R.id.signup_button_send);
        email = findViewById(R.id.signup_field_email);
        password = findViewById(R.id.signup_field_password);
        passwordRepeat = findViewById(R.id.signup_field_passwordrepeat);
        progressBar = findViewById(R.id.progressBar);
        useConditions = findViewById(R.id.signup_checkbox_useconditions);
        privacyPolicies = findViewById(R.id.signup_checkbox_privacypolicies);

        // Listeners
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                signUp();
            }
        });

        // Comprovem e-mail
        email.addTextChangedListener(new TextWatcher()  {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s)  {
                if (!TextUtils.isEmpty(email.getText().toString()) && !Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
                    enableSendButton(false);
                    emailOk = false;
                    email.setError(getString(R.string.recover_email_not_valid));
                } else {
                    if (TextUtils.isEmpty(email.getText().toString())) {
                        enableSendButton(false);
                        emailOk = false;
                    } else {
                        emailOk = true;
                        checkFields();
                    }

                    email.setError(null);
                }
            }
        });

        password.addTextChangedListener(new TextWatcher()  {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s)  {
                if (TextUtils.isEmpty(password.getText().toString()) && !passwordRepeat.getText().toString().equals(password.getText().toString())) {
                    enableSendButton(false);
                    passwordOk = false;
                } else {
                    passwordOk = true;
                    checkFields();
                }
            }
        });

        passwordRepeat.addTextChangedListener(new TextWatcher()  {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s)  {
                if (!TextUtils.isEmpty(passwordRepeat.getText().toString()) && !passwordRepeat.getText().toString().equals(password.getText().toString())) {
                    enableSendButton(false);
                    passwordRepeatOk = false;
                    passwordRepeat.setError(getString(R.string.signup_password_not_equal));
                } else {
                    if (TextUtils.isEmpty(passwordRepeat.getText().toString())) {
                        enableSendButton(false);
                        passwordRepeatOk = false;
                    } else {
                        passwordRepeatOk = true;
                        checkFields();
                    }

                    passwordRepeat.setError(null);
                }
            }
        });

        useConditions.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                useConditionsOk = isChecked;
                checkFields();
            }
        });

        privacyPolicies.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                privacyPoliciesOk = isChecked;
                checkFields();
            }
        });

    }

    private void signUp() {

        final Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showProgress(false);

                User user = JsonUtil.deserializeToObject(response, User.class);

                // Guardem les dades de l'usuari
                AccountHelper.setUserData(getApplicationContext(), user.getEmail(), user.getUserId().toString(), user.getAvatar());

                // Anem a pantalla principal un cop OK
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finishActivity();
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                    ResourcesUtil.showError(getApplicationContext(), getApplicationContext().getPackageName(), response.data);
                }
            }
        };

        // Usuari
        User user = new User();
        user.setEmail(email.getText().toString());
        user.setPassword(password.getText().toString());

        // Enviem petició
        new DefaultApi().createUser(user, responseListener, errorListener);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgress(boolean show) {
        enableSendButton(!show);
        enableFields(!show);

        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    private void enableSendButton(boolean enable) {
        sendButton.setEnabled(enable);
    }

    private void enableFields(boolean enable) {
        email.setEnabled(enable);
        password.setEnabled(enable);
        passwordRepeat.setEnabled(enable);
        useConditions.setEnabled(enable);
        privacyPolicies.setEnabled(enable);
    }

    private void checkFields() {
        Log.i("checkFields", String.valueOf(emailOk && passwordOk && passwordRepeatOk && useConditionsOk && privacyPoliciesOk));
        if (emailOk && passwordOk && passwordRepeatOk && useConditionsOk && privacyPoliciesOk)
            enableSendButton(true);
    }

    private void finishActivity() {
        this.finishAffinity();
    }

}
