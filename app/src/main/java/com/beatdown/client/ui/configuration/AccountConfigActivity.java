/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.configuration;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beatdown.client.R;
import com.beatdown.client.api.DefaultApi;
import com.beatdown.client.configuration.AccountHelper;
import com.beatdown.client.configuration.LocaleHelper;
import com.beatdown.client.model.User;
import com.beatdown.client.ui.login.LoginActivity;
import com.beatdown.client.utils.ImageUtil;
import com.beatdown.client.utils.ResourcesUtil;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class AccountConfigActivity extends AppCompatActivity {

    private EditText pwdField, pwdFieldRepeat;
    private Button sendButton;
    private ImageButton avatarButton;
    private ConstraintLayout progressBar, accountPanel, languagePanel;
    private Spinner languageSpinner;
    private BottomNavigationView bottomNavigationView;
    private boolean passwordOk = false, passwordRepeatOk = false, avatarOk = false;

    // Data
    private final List<String> locale = new ArrayList<String>() {{
        add(LocaleHelper.CA);
        add(LocaleHelper.ES);
        add(LocaleHelper.EN);
    }};
    private final int GALLERY = 1, CAMERA = 2;
    private static final String IMAGE_DIRECTORY = "/beatdown";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_configuration_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.accountconfig_title);

        requestMultiplePermissions();

        // Elements
        avatarButton = findViewById(R.id.accountconfig_button_avatar);
        pwdField = findViewById(R.id.accountconfig_field_password);
        pwdFieldRepeat = findViewById(R.id.accountconfig_field_passwordrepeat);
        sendButton = findViewById(R.id.accountconfig_button_save);
        Button langButton = findViewById(R.id.accountconfig_button_save2);
        TextView removeLabel = findViewById(R.id.accountconfig_link_delete);
        languageSpinner = findViewById(R.id.accountconfig_spinner_language2);
        progressBar = findViewById(R.id.progressBar);
        accountPanel = findViewById(R.id.accountconfig_account_panel);
        languagePanel = findViewById(R.id.accountconfig_language_panel);

        showProgress(true);

        // Listener menú top
        bottomNavigationView = findViewById(R.id.accountconfig_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.accountconfig_tab_account:
                        accountPanel.setVisibility(View.VISIBLE);
                        languagePanel.setVisibility(View.INVISIBLE);
                        break;

                    case R.id.accountconfig_tab_language:
                        languagePanel.setVisibility(View.VISIBLE);
                        accountPanel.setVisibility(View.INVISIBLE);
                        break;
                }
                return true;
            }
        });

        // Comprovem dades guardades
        checkSavedConfig();

        // Listeners
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

        langButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveLang();
            }
        });

        pwdField.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(pwdField.getText().toString()) && !pwdFieldRepeat.getText().toString().equals(pwdField.getText().toString())) {
                    enableSendButton(false);
                    passwordOk = false;
                } else {
                    passwordOk = true;
                    checkFields();
                }
            }
        });

        pwdFieldRepeat.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(pwdFieldRepeat.getText().toString()) && !pwdFieldRepeat.getText().toString().equals(pwdField.getText().toString())) {
                    enableSendButton(false);
                    passwordRepeatOk = false;
                    pwdFieldRepeat.setError(getString(R.string.signup_password_not_equal));
                } else {
                    if (TextUtils.isEmpty(pwdFieldRepeat.getText().toString())) {
                        enableSendButton(false);
                        passwordRepeatOk = false;
                    } else {
                        passwordRepeatOk = true;
                        checkFields();
                    }

                    pwdFieldRepeat.setError(null);
                }
            }
        });

        removeLabel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                removeAccountAlert();
            }
        });

        avatarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

    }

    /**
     * Diàleg foto.
     */
    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle(getString(R.string.accountconfig_label_avatartitle));
        String[] pictureDialogItems = {
                getString(R.string.action_msg_from_gallery),
                getString(R.string.action_msg_from_camera)};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallery();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    saveImage(bitmap);
                    avatarButton.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            avatarButton.setImageBitmap(thumbnail);
            saveImage(thumbnail);
        }
    }

    private String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();

            enableSendButton(true);
            avatarOk = true;

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private void requestMultiplePermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            finish();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                    }
                })
                .onSameThread()
                .check();
    }

    /**
     * Recupera configuració guardada.
     */
    private void checkSavedConfig() {
        // Idioma
        String lang = LocaleHelper.getLanguage(getApplicationContext());
        languageSpinner.setSelection(locale.indexOf(lang));

        // Avatar
        String avatar = AccountHelper.getUserAvatar(getApplicationContext());

        if (avatar != null) {
            avatarButton.setImageBitmap(ImageUtil.fromBase64ToBitmap(avatar));
        }

        // Comprovem si l'usuari està logejat
        if (!AccountHelper.hasUserData(getApplicationContext())) {
            accountPanel.setVisibility(View.INVISIBLE);
            languagePanel.setVisibility(View.VISIBLE);
            bottomNavigationView.getMenu().findItem(R.id.accountconfig_tab_account).setVisible(false);
            bottomNavigationView.getMenu().findItem(R.id.accountconfig_tab_language).setChecked(true);
        }

        showProgress(false);
    }

    /**
     * Guarda la configuració.
     */
    private void saveLang() {
        // Guardem la configuració
        String lang = this.locale.get(languageSpinner.getSelectedItemPosition());

        // En cas que l'idioma sigui diferent, actualitzem
        if (!lang.equalsIgnoreCase(LocaleHelper.getLanguage(getApplicationContext()))) {
            LocaleHelper.setLocale(getApplicationContext(), lang);
            updateLanguageApp(lang);
        }
    }

    /**
     * Recuperem categories via API.
     */
    private void save() {

        showProgress(true);

        final Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // Guardem avatar
                if (avatarButton.getDrawable() instanceof BitmapDrawable) {
                    String avatar = ImageUtil.fromBitmapToBase64(((BitmapDrawable) avatarButton.getDrawable()).getBitmap());
                    AccountHelper.setAccountAvatar(getApplicationContext(), avatar);
                }

                showProgress(false);
                Toast.makeText(getApplicationContext(), R.string.action_msg_saved, Toast.LENGTH_SHORT).show();
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    ResourcesUtil.showError(getApplicationContext(), getApplicationContext().getPackageName(), response.data);
                }
            }
        };

        User user = new User();
        if (avatarButton.getDrawable() instanceof BitmapDrawable)
            user.setAvatar(ImageUtil.fromBitmapToBase64(((BitmapDrawable) avatarButton.getDrawable()).getBitmap()));
        user.setPassword(pwdField.getText().toString());

        // Cridem API
        new DefaultApi().editUser(Integer.valueOf(AccountHelper.getUserId(getApplicationContext())), user, responseListener, errorListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgress(boolean show) {
        enableSendButton(!show);

        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.INVISIBLE);
    }

    private void enableSendButton(boolean enable) {
        sendButton.setEnabled(enable);
    }

    private void checkFields() {
        if ((passwordOk && passwordRepeatOk) || avatarOk)
            enableSendButton(true);
    }

    /**
     * Mètode que actualitza l'idioma de l'app.
     */
    private void updateLanguageApp(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, AccountConfigActivity.class);
        startActivity(refresh);
        NavUtils.navigateUpFromSameTask(this);
    }

    /**
     * Diàleg per eliminar compte.
     */
    private void removeAccountAlert() {
        showProgress(true);

        // Mostrem alerta
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.accountconfig_link_delete));
        alertDialog.setMessage(getString(R.string.action_msg_are_you_sure));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.action_msg_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        removeAccount();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.action_msg_no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        showProgress(false);
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void removeAccount() {

        final Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showProgress(false);

                // Esborrem dades usuari
                AccountHelper.eraseUserData(getApplicationContext());

                // Anem a pantalla de login
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finishAffinity();
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    ResourcesUtil.showError(getApplicationContext(), getApplicationContext().getPackageName(), response.data);
                }
            }
        };

        // Cridem API
        new DefaultApi().deleteUser(Integer.valueOf(AccountHelper.getUserId(getApplicationContext())), responseListener, errorListener);
    }

}
