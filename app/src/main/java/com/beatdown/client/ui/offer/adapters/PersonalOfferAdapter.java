/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.offer.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beatdown.client.R;
import com.beatdown.client.configuration.LocaleHelper;
import com.beatdown.client.model.PersonalOffer;

import java.util.List;

public class PersonalOfferAdapter extends RecyclerView.Adapter<PersonalOfferAdapter.MyViewHolder> {

    public static List<PersonalOffer> mDataset;
    private Context context;
    private static RecyclerViewClickListener itemListener;

    public interface RecyclerViewClickListener {
        void recyclerViewListClicked(View v, int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;

        MyViewHolder(LinearLayout v) {
            super(v);
            textView = v.findViewById(R.id.personaldiscounts_item_desc);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getLayoutPosition());
        }
    }

    public PersonalOfferAdapter(List<PersonalOffer> mDataset, Context context, RecyclerViewClickListener itemListener) {
        this.mDataset = mDataset;
        this.context = context;
        this.itemListener = itemListener;
    }

    // Crea noves views
    @Override
    @NonNull
    public PersonalOfferAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                                int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.personaldiscounts_discount_item, parent, false);

        return new MyViewHolder((LinearLayout) layout);
    }

    // Reemplaça contingut
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        // Ajustem idioma
        String code = LocaleHelper.getLanguage(context);
        String desc;

        if (LocaleHelper.EN.equalsIgnoreCase(code))
            desc = mDataset.get(position).getOffer().getTitleEng();
        else if (LocaleHelper.ES.equalsIgnoreCase(code))
            desc = mDataset.get(position).getOffer().getTitleSpa();
        else
            desc = mDataset.get(position).getOffer().getTitleCat();

        holder.textView.setText(desc);
    }

    // Mida del dataset
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}