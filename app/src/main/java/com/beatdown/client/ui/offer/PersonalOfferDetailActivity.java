/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.offer;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beatdown.client.R;
import com.beatdown.client.api.DefaultApi;
import com.beatdown.client.configuration.AccountHelper;
import com.beatdown.client.configuration.LocaleHelper;
import com.beatdown.client.model.PersonalOffer;
import com.beatdown.client.ui.offer.adapters.PersonalOfferAdapter;
import com.beatdown.client.utils.ResourcesUtil;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;

public class PersonalOfferDetailActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView expirationDate, title, distance, discount, description;
    private Button exchangeButton;
    private ConstraintLayout progressBar;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personaldiscount_detail_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Elements
        imageView = findViewById(R.id.personaldiscountdetail_image);
        expirationDate = findViewById(R.id.personaldiscountdetail_label_expiration);
        title = findViewById(R.id.personaldiscountdetail_label_title);
        distance = findViewById(R.id.personaldiscountdetail_label_category_distance);
        discount = findViewById(R.id.personaldiscountdetail_label_discount);
        description = findViewById(R.id.personaldiscountdetail_label_description);
        exchangeButton = findViewById(R.id.personaldiscountdetail_button_exchange);
        progressBar = findViewById(R.id.progressBar);

        showProgress(true);

        // Recuperem ID descompte
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        position = extras.getInt(Intent.EXTRA_TEXT);

        // Recuperem oferta
        final PersonalOffer offer = PersonalOfferAdapter.mDataset.get(position);

        // Init
        init(offer);

        // Listeners
        exchangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                exchange(offer.getPersonalOfferId());
            }
        });

        showProgress(false);
    }

    /**
     * Mètode que ajusta totes les dades.
     *
     * @param offer
     */
    private void init(PersonalOffer offer) {
        expirationDate.setText(String.format("%s %s", getString(R.string.discountdetail_label_valid), new SimpleDateFormat("dd/MM/yyyy").format(offer.getExpirationDate())));
        title.setText(offer.getStablishment().getName());

        String category;
        String desc;

        // Comprovem idioma per saber què ajustar
        if (LocaleHelper.getLanguage(getApplicationContext()).equalsIgnoreCase(LocaleHelper.EN)) {
            category = offer.getStablishment().getCategory().getDescriptionEng();
            desc = offer.getOffer().getTitleEng();
        } else if (LocaleHelper.getLanguage(getApplicationContext()).equalsIgnoreCase(LocaleHelper.ES)) {
            category = offer.getStablishment().getCategory().getDescriptionSpa();
            desc = offer.getOffer().getTitleSpa();
        } else {
            category = offer.getStablishment().getCategory().getDescriptionCat();
            desc = offer.getOffer().getTitleCat();
        }

        distance.setText(String.format("%s · %s km", category, offer.getOffer().getDistance()));
        description.setText(desc);

        // Tractem el tipus de descompte
        String discountV = offer.getOffer().getDiscount() + "";

        if (discountV.startsWith("-"))
            discountV += "€";
        else
            discountV += "%";

        discount.setText(discountV);

        // Ajustem imatge
        Picasso.get().load(DefaultApi.basePath + offer.getOffer().getImageUrl()).into(imageView);
    }

    /**
     * Crida a l'API per marcar-lo com usat.
     *
     * @param id
     */
    private void exchange(int id) {

        final Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showProgress(false);
                Intent intent = new Intent(getApplicationContext(), ExchangeOfferDetailActivity.class);
                intent.putExtra(Intent.EXTRA_TEXT, position);
                startActivity(intent);
                finish();
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    ResourcesUtil.showError(getApplicationContext(), getApplicationContext().getPackageName(), response.data);
                }
            }
        };

        // Enviem petició
        new DefaultApi().deletePersonalOffer(AccountHelper.getUserId(getApplicationContext()), id, responseListener, errorListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgress(boolean show) {
        enableSendButton(!show);

        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    private void enableSendButton(boolean enable) {
        exchangeButton.setEnabled(enable);
    }

}
