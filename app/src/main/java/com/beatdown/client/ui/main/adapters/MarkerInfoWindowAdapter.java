/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.main.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.beatdown.client.R;
import com.beatdown.client.api.DefaultApi;
import com.beatdown.client.configuration.AccountHelper;
import com.beatdown.client.configuration.LocaleHelper;
import com.beatdown.client.model.Offer;
import com.beatdown.client.ui.main.MainActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Picasso;

public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private Context ctx;

    public MarkerInfoWindowAdapter(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity) ctx).getLayoutInflater().inflate(R.layout.discount_detail_marker_activity, null);

        ImageView image, fav;
        TextView title, category, desc, discount;

        image = view.findViewById(R.id.discountmarker_image);
        fav = view.findViewById(R.id.discountmarker_button_fav);
        title = view.findViewById(R.id.discountmarker_label_title);
        category = view.findViewById(R.id.discountmarker_label_category_distance);
        desc = view.findViewById(R.id.discountmarker_label_description);
        discount = view.findViewById(R.id.discountmarker_label_discount);

        // Obtenim oferta
        Offer offer = MainActivity.offerList.get((int) marker.getTag());

        // Labels
        title.setText(offer.getStablishment().getName());

        String cat = offer.getStablishment().getCategory().getDescriptionCat();
        String descr = offer.getTitleCat();

        // Traduïm segons idioma
        if (LocaleHelper.getLanguage(ctx).equalsIgnoreCase(LocaleHelper.EN)) {
            cat = offer.getStablishment().getCategory().getDescriptionEng();
            descr = offer.getTitleEng();
        } else if (LocaleHelper.getLanguage(ctx).equalsIgnoreCase(LocaleHelper.ES)) {
            cat = offer.getStablishment().getCategory().getDescriptionSpa();
            descr = offer.getTitleSpa();
        }

        category.setText(String.format("%s · %s km", cat, offer.getDistance()));
        desc.setText(descr);

        // Tractem el tipus de descompte
        String discountV = offer.getDiscount() + "";

        if (discountV.startsWith("-"))
            discountV += "€";
        else
            discountV += "%";

        discount.setText(discountV);

        // Imatges
        Picasso.get().load(DefaultApi.basePath + offer.getImageUrl()).into(image);
        if (AccountHelper.hasUserData(ctx))
            fav.setImageResource(offer.getFavorite() ? R.drawable.button_fav : R.drawable.button_fav_unmarked);
        else
            fav.setVisibility(View.INVISIBLE);

        return view;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        // TODO Auto-generated method stub
        return null;
    }

}