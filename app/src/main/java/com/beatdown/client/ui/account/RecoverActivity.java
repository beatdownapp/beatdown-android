/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.account;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beatdown.client.R;
import com.beatdown.client.api.DefaultApi;
import com.beatdown.client.configuration.LocaleHelper;
import com.beatdown.client.utils.ResourcesUtil;

public class RecoverActivity extends AppCompatActivity {

    private Button sendButton;
    private EditText email;
    private ConstraintLayout progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recover_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Elements
        sendButton = findViewById(R.id.recover_button_send);
        email = findViewById(R.id.recover_field_email);
        progressBar = findViewById(R.id.progressBar);

        // Listeners
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                sendRecoverEmail(email.getText().toString());
            }
        });

        // Comprovem e-mail
        email.addTextChangedListener(new TextWatcher()  {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s)  {
                if (!TextUtils.isEmpty(email.getText().toString()) && !Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
                    enableSendButton(false);
                    email.setError(getString(R.string.recover_email_not_valid));
                } else {
                    if (TextUtils.isEmpty(email.getText().toString()))
                        enableSendButton(false);
                    else
                        enableSendButton(true);

                    email.setError(null);
                }
            }
        });
    }

    private void sendRecoverEmail(String email) {

        final Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showProgress(false);
                Toast.makeText(getApplicationContext(), getString(R.string.recover_email_sent), Toast.LENGTH_LONG).show();
                finishActivity();
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                    ResourcesUtil.showError(getApplicationContext(), getApplicationContext().getPackageName(), response.data);
                }
            }
        };

        // Enviem petició
        new DefaultApi().recoverAccount(email, LocaleHelper.getLanguage(this), responseListener, errorListener);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgress(boolean show) {
        enableSendButton(!show);
        enableFields(!show);

        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    private void enableSendButton(boolean enable) {
        sendButton.setEnabled(enable);
    }

    private void enableFields(boolean enable) {
        email.setEnabled(enable);
    }

    private void finishActivity() {
        this.finish();
    }

}
