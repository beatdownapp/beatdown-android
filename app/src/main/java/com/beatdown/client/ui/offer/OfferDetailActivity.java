/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.offer;

import android.accounts.Account;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.beatdown.client.R;
import com.beatdown.client.api.DefaultApi;
import com.beatdown.client.configuration.AccountHelper;
import com.beatdown.client.configuration.LocaleHelper;
import com.beatdown.client.model.Offer;
import com.beatdown.client.ui.main.MainActivity;
import com.beatdown.client.utils.ResourcesUtil;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

public class OfferDetailActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView title, distance, discount, description;
    private ImageButton favButton;
    private ConstraintLayout progressBar;
    private int position;
    private String origin;
    private Offer offer;
    private boolean offerFav = false, fromFav = false, fromMain = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discount_detail_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Elements
        imageView = findViewById(R.id.discountdetail_image);
        title = findViewById(R.id.discountdetail_label_title);
        distance = findViewById(R.id.discountdetail_label_category_distance);
        discount = findViewById(R.id.discountdetail_label_discount);
        description = findViewById(R.id.discountdetail_label_description);
        favButton = findViewById(R.id.discountdetail_button_fav);
        progressBar = findViewById(R.id.progressBar);

        showProgress(true);

        // Recuperem ID descompte
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        position = extras.getInt(Intent.EXTRA_TEXT);
        origin = extras.getString(Intent.EXTRA_UID);

        if (origin == null)
            finish();

        // Recuperem oferta
        if (origin.equalsIgnoreCase(FavoriteOfferActivity.UID_FAVORITE)) {
            offer = FavoriteOfferActivity.offerList.get(position);
            fromFav = true;
        } else if (origin.equalsIgnoreCase(MainActivity.UID_MAIN)) {
            offer = MainActivity.offerList.get(position);
            fromMain = true;
        } else
            finish();

        // Init
        init(offer);

        if (!AccountHelper.hasUserData(getApplicationContext()))
            favButton.setVisibility(View.INVISIBLE);

        // Listeners
        favButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                toggleFavorite(offer.getOfferId());
            }
        });

        initAd();

        showProgress(false);
    }

    /**
     * Inicialitza anunci.
     */
    private void initAd() {
        AdView mAdView = (AdView) findViewById(R.id.adView);

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice(getString(R.string.test_device_code)) // Device de proves!
                .build();

        mAdView.loadAd(adRequest);
    }

    /**
     * Mètode que ajusta totes les dades.
     *
     * @param offer
     */
    private void init(Offer offer) {
        title.setText(offer.getStablishment().getName());

        String category;
        String desc;

        // Comprovem idioma per saber què ajustar
        if (LocaleHelper.getLanguage(getApplicationContext()).equalsIgnoreCase(LocaleHelper.EN)) {
            category = offer.getStablishment().getCategory().getDescriptionEng();
            desc = offer.getTitleEng();
        } else if (LocaleHelper.getLanguage(getApplicationContext()).equalsIgnoreCase(LocaleHelper.ES)) {
            category = offer.getStablishment().getCategory().getDescriptionSpa();
            desc = offer.getTitleSpa();
        } else {
            category = offer.getStablishment().getCategory().getDescriptionCat();
            desc = offer.getTitleCat();
        }

        distance.setText(String.format("%s · %s km", category, offer.getDistance()));
        description.setText(desc);

        // Tractem el tipus de descompte
        String discountV = offer.getDiscount() + "";

        if (discountV.startsWith("-"))
            discountV += "€";
        else
            discountV += "%";

        discount.setText(discountV);

        // Ajustem imatge
        Picasso.get().load(DefaultApi.basePath + offer.getImageUrl()).into(imageView);

        // Ajustem estat del botó
        offerFav = offer.getFavorite();
        if (offerFav)
            favButton.setImageResource(R.drawable.button_fav);
    }

    /**
     * Crida a l'API per marcar-lo com usat.
     *
     * @param id
     */
    private void toggleFavorite(int id) {

        final Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (offerFav) {
                    favButton.setImageResource(R.drawable.button_fav_unmarked);
                    Toast.makeText(getApplicationContext(), getString(R.string.action_msg_unmarked_fav), Toast.LENGTH_SHORT).show();
                } else {
                    favButton.setImageResource(R.drawable.button_fav);
                    Toast.makeText(getApplicationContext(), getString(R.string.action_msg_marked_fav), Toast.LENGTH_SHORT).show();
                }

                offerFav = !offerFav;

                showProgress(false);
            }
        };

        final Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    ResourcesUtil.showError(getApplicationContext(), getApplicationContext().getPackageName(), response.data);
                }
            }
        };

        // Enviem petició
        new DefaultApi().markAsFavorite(AccountHelper.getUserId(getApplicationContext()), id, responseListener, errorListener);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (fromFav) {
                    NavUtils.navigateUpTo(this, new Intent(getApplicationContext(), FavoriteOfferActivity.class));
                    finish();
                } else if (fromMain) {
                    NavUtils.navigateUpTo(this, new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgress(boolean show) {
        enableFavButton(!show);

        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    private void enableFavButton(boolean enable) {
        favButton.setEnabled(enable);
    }

}
