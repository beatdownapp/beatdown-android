/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.offer;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.beatdown.client.R;
import com.beatdown.client.model.PersonalOffer;
import com.beatdown.client.ui.offer.adapters.PersonalOfferAdapter;
import com.google.zxing.WriterException;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class ExchangeOfferDetailActivity extends AppCompatActivity {

    private ImageView imageView;
    private ConstraintLayout progressBar;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exchangepersonaldiscount_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Elements
        imageView = findViewById(R.id.exchangepersonaldiscount_image_qr);
        progressBar = findViewById(R.id.progressBar);

        showProgress(true);

        // Recuperem ID descompte
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        position = extras.getInt(Intent.EXTRA_TEXT);

        // Recuperem oferta
        final PersonalOffer offer = PersonalOfferAdapter.mDataset.get(position);

        // L'eliminem ja que està marcada com usada
        PersonalOfferAdapter.mDataset.remove(position);

        generate(offer.getCode());

        showProgress(false);
    }

    /**
     * Genera el QR per escanejar.
     *
     * @param code
     */
    private void generate(String code) {
        QRGEncoder qrgEncoder = new QRGEncoder(code, null, QRGContents.Type.TEXT, getDimension());

        try {
            // Getting QR-Code as Bitmap
            Bitmap bitmap = qrgEncoder.encodeAsBitmap();
            // Setting Bitmap to ImageView
            imageView.setImageBitmap(bitmap);
        } catch (WriterException e) {
            Log.v("Parse error", e.toString());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgress(boolean show) {
        if (show)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    /**
     * Mètode que calcula la mida del codi QR.
     *
     * @return
     */
    private int getDimension() {
        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        return smallerDimension * 3 / 4;
    }

}
