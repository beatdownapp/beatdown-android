/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.notification.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beatdown.client.R;
import com.beatdown.client.configuration.LocaleHelper;
import com.beatdown.client.model.Notification;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private List<Notification> mDataset;
    private Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView desc, date;

        MyViewHolder(ConstraintLayout v) {
            super(v);
            desc = v.findViewById(R.id.notifications_item_desc);
            date = v.findViewById(R.id.notifications_item_date);
        }
    }

    public NotificationAdapter(List<Notification> mDataset, Context context) {
        this.mDataset = mDataset;
        this.context = context;
    }

    // Crea noves views
    @Override
    @NonNull
    public NotificationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                               int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notifications_item, parent, false);

        return new MyViewHolder((ConstraintLayout) layout);
    }

    // Reemplaça contingut
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        // Ajustem idioma
        String code = LocaleHelper.getLanguage(context);
        String desc;

        if (LocaleHelper.EN.equalsIgnoreCase(code))
            desc = mDataset.get(position).getTitleEng();
        else if (LocaleHelper.ES.equalsIgnoreCase(code))
            desc = mDataset.get(position).getTitleSpa();
        else
            desc = mDataset.get(position).getTitleCat();

        holder.desc.setText(desc);
        holder.date.setText(new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE).format(mDataset.get(position).getCreationDate()));
    }

    // Mida del dataset
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}