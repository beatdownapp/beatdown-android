/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.ui.configuration.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.beatdown.client.R;
import com.beatdown.client.configuration.LocaleHelper;
import com.beatdown.client.model.StablishmentCategory;

import java.util.List;
import java.util.Set;

public class SearchConfigCategoriesAdapter extends RecyclerView.Adapter<SearchConfigCategoriesAdapter.MyViewHolder> {

    private List<StablishmentCategory> mDataset;
    private Context context;
    private Set<String> selectedCategories;

    public interface OnItemCheckListener {
        void onItemCheck(StablishmentCategory item);

        void onItemUncheck(StablishmentCategory item);
    }

    @NonNull
    private OnItemCheckListener onItemCheckListener;


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private CheckBox check;

        private MyViewHolder(LinearLayout v) {
            super(v);
            check = v.findViewById(R.id.categories_list_option);
            check.setClickable(false);
        }

        private void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }
    }

    public SearchConfigCategoriesAdapter(List<StablishmentCategory> mDataset, Set<String> selectedCategories, Context context, @NonNull OnItemCheckListener onItemCheckListener) {
        this.mDataset = mDataset;
        this.context = context;
        this.onItemCheckListener = onItemCheckListener;
        this.selectedCategories = selectedCategories;
    }

    // Crea noves views
    @Override
    public SearchConfigCategoriesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                         int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.categories_item, parent, false);

        return new MyViewHolder((LinearLayout) layout);
    }

    // Reemplaça contingut
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        // Ajustem idioma
        String code = LocaleHelper.getLanguage(context);
        String desc;

        if (LocaleHelper.EN.equalsIgnoreCase(code))
            desc = mDataset.get(position).getDescriptionEng();
        else if (LocaleHelper.ES.equalsIgnoreCase(code))
            desc = mDataset.get(position).getDescriptionSpa();
        else
            desc = mDataset.get(position).getDescriptionCat();

        holder.check.setText(desc);

        // Marquem en cas de que el tinguem guardat
        if (selectedCategories!= null && !selectedCategories.isEmpty() && selectedCategories.contains(mDataset.get(position).getStablishmentCategoryId().toString())) {
            holder.check.setChecked(true);
        }

        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.check.setChecked(!holder.check.isChecked());

                if (holder.check.isChecked()) {
                    onItemCheckListener.onItemCheck(mDataset.get(position));
                } else {
                    onItemCheckListener.onItemUncheck(mDataset.get(position));
                }
            }
        });
    }

    // Mida del dataset
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}