/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.configuration;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class ConfigurationHelper {

    private static final String SEARCH_CONFIG_DISTANCE = "Config.Search.Distance";
    private static final String SEARCH_CONFIG_CATEGORIES = "Config.Search.Categories";

    public static HashMap<String, Object> getSearchConfig(Context context) {
        return getPersistedData(context);
    }

    public static Context setSearchConfig(Context context, int distance, Set<String> categories) {
        persist(context, distance, categories);
        return context;
    }

    public static int getDistance(Context context) {
        return (int) getPersistedData(context).get(SEARCH_CONFIG_DISTANCE);
    }

    public static Set<String> getCategories(Context context) {
        Set<String> categories = (Set<String>) getPersistedData(context).get(SEARCH_CONFIG_CATEGORIES);

        if (categories == null)
            return new HashSet<>();

        return categories;
    }

    private static HashMap<String, Object> getPersistedData(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        HashMap<String, Object> userData = new HashMap<>();
        userData.put(SEARCH_CONFIG_DISTANCE, preferences.getInt(SEARCH_CONFIG_DISTANCE, 0));
        userData.put(SEARCH_CONFIG_CATEGORIES, preferences.getStringSet(SEARCH_CONFIG_CATEGORIES, null));

        return userData;
    }

    private static void persist(Context context, int distance, Set<String> categories) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putInt(SEARCH_CONFIG_DISTANCE, distance);
        editor.putStringSet(SEARCH_CONFIG_CATEGORIES, categories);
        editor.apply();
    }

}
