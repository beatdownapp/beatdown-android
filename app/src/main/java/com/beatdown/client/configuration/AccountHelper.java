/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.configuration;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.HashMap;

public class AccountHelper {

    private static final String ACCOUNT_EMAIL = "Account.Email";
    private static final String ACCOUNT_USER_ID = "Account.User.Id";
    private static final String ACCOUNT_AVATAR = "Account.Avatar";

    public static HashMap<String, String> getUserData(Context context) {
        return getPersistedData(context);
    }

    public static Context setUserData(Context context, String email, String userId, String avatar) {
        persist(context, email, userId, avatar);
        return context;
    }

    public static boolean hasUserData(Context context) {
        return getPersistedData(context).get(ACCOUNT_USER_ID) != null;
    }

    public static String getUserId(Context context) {
        return getPersistedData(context).get(ACCOUNT_USER_ID);
    }

    public static String getUserEmail(Context context) {
        return getPersistedData(context).get(ACCOUNT_EMAIL);
    }

    public static String getUserAvatar(Context context) {
        return getPersistedData(context).get(ACCOUNT_AVATAR);
    }

    private static HashMap<String, String> getPersistedData(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        HashMap<String, String> userData = new HashMap<>();
        userData.put(ACCOUNT_EMAIL, preferences.getString(ACCOUNT_EMAIL, null));
        userData.put(ACCOUNT_USER_ID, preferences.getString(ACCOUNT_USER_ID, null));
        userData.put(ACCOUNT_AVATAR, preferences.getString(ACCOUNT_AVATAR, null));

        return userData;
    }

    private static void persist(Context context, String email, String userId, String avatar) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(ACCOUNT_EMAIL, email);
        editor.putString(ACCOUNT_USER_ID, userId);
        editor.putString(ACCOUNT_AVATAR, avatar);
        editor.apply();
    }

    public static void setAccountAvatar(Context context, String avatar) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(ACCOUNT_AVATAR, avatar);
        editor.apply();
    }

    public static void eraseUserData(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        editor.remove(ACCOUNT_USER_ID);
        editor.remove(ACCOUNT_EMAIL);
        editor.remove(ACCOUNT_AVATAR);
        editor.apply();
    }

}
