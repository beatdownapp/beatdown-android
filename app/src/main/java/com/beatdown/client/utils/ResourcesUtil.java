/*
 * Copyright (C) 2019 Andreu van Walré Fernández. All rights reserved.
 */

package com.beatdown.client.utils;

import android.content.Context;
import android.widget.Toast;

import com.beatdown.client.model.ApiError;

public class ResourcesUtil {

    public static void showError(Context context, String packageName, byte[] data){
        // Transformem
        String errorCode = ((ApiError) JsonUtil.deserializeToObject(new String(data), ApiError.class)).getErrorCode();

        // Mostrem l'error
        Toast.makeText(context, context.getResources().getIdentifier(errorCode, "string", packageName), Toast.LENGTH_LONG).show();
    }

}
